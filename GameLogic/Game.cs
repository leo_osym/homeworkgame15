﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLogic
{
    public class Game
    {
        private Random rnd = new Random();
        private int[,] _gameField = new int[4, 4];
        public int zeroX { get; private set; }
        public int zeroY { get; private set; }

        public int this[int x, int y]
        {
            get => _gameField[x, y];
        }

        public void zeroUp()
        {
            if (zeroY == 3) return;

            _gameField[zeroX, zeroY] = _gameField[zeroX, zeroY + 1];
            ++zeroY;
            _gameField[zeroX, zeroY] = 0;

        }
        public void zeroDown()
        {
            if (zeroY == 0) return;

            _gameField[zeroX, zeroY] = _gameField[zeroX, zeroY - 1];
            --zeroY;
            _gameField[zeroX, zeroY] = 0;
        }

        public void zeroLeft()
        {
            if (zeroX == 3) return;

            _gameField[zeroX, zeroY] = _gameField[zeroX + 1, zeroY];
            ++zeroX;
            _gameField[zeroX, zeroY] = 0;
        }
        public void zeroRight()
        {
            if (zeroX == 0) return;

            _gameField[zeroX, zeroY] = _gameField[zeroX - 1, zeroY];
            --zeroX;
            _gameField[zeroX, zeroY] = 0;
        }



        public Game(int flag)
        {
            InitGame(flag);
        }

        public void InitGame(int flag)
        {
            //int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 15 }; // for testing purpose
            int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0 };

            Shuffle(flag, arr);

            for (int n = 0, i = 0; i < 4; ++i)
            {
                for (int j = 0; j < 4; ++j, ++n)
                {
                    if (flag == 1)
                    {
                        _gameField[j, i] = arr[n];
                        if (arr[n] == 0)
                        {
                            zeroX = j;
                            zeroY = i;
                        }
                    }
                    else
                    {
                        _gameField[i, j] = arr[n];
                        if (arr[n] == 0)
                        {
                            zeroX = i;
                            zeroY = j;
                        }
                    }
                }
            }
        }

        public bool CheckAndGo(int value)
        {
            if(zeroX > 0 && _gameField[zeroX-1, zeroY]==value)
            {
                zeroRight();
                return true;
            }
            if (zeroX <3  && _gameField[zeroX + 1, zeroY] == value)
            {
                zeroLeft();
                return true;
            }
            if (zeroY > 0 && _gameField[zeroX, zeroY - 1] == value)
            {
                zeroDown();
                return true;
            }
            if (zeroY < 3 && _gameField[zeroX, zeroY + 1] == value)
            {
                zeroUp();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Shuffling the array
        /// </summary>
        /// <param name="arr">Array with elements uses in game</param>
        private void Shuffle(int[] arr)
        {
            for (int i = 0; i < arr.Length; ++i)
            {
                int r = rnd.Next(arr.Length);
                int tmp = arr[i];
                arr[i] = arr[r];
                arr[r] = tmp;
            }
        }

        /// <summary>
        /// Shuffling depends on flag and ability to win with current combination
        /// </summary>
        /// <param name="flag">Represent type of game</param>
        /// <param name="arr">Array with elements uses in game</param>
        private void Shuffle(int flag, int[] arr)
        {
            Shuffle(arr);
            if (flag == 1)
            {
                while (!IsWinCombination(arr))
                {
                    Shuffle(arr);
                }
            }
            if (flag == 2 | flag == 3)
            {
                while (IsWinCombination(arr))
                {
                    Shuffle(arr);
                }
            }
        }
        public bool IsWinCombination(int[] arr)
        {
            int count = 0, sum = 0, e = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                count = 0;
                for (int j = i; j < arr.Length; j++)
                {
                    if (arr[i] > arr[j] && arr[i] != 0 && arr[j] != 0)
                    {
                        count++;
                    }
                }
                sum += count;
            }
            int ind = Array.IndexOf(arr, 0) + 1;
            if (ind <= 4) e = 1;
            if (ind > 4 && ind <= 8) e = 2;
            if (ind > 8 && ind <= 12) e = 3;
            if (ind > 12) e = 4;
            sum += e;
            if (sum % 2 == 0) return true;
            else return false;
        }
        public bool IsWin(int flag)
        {
            int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0 };
            for (int n = 0, i = 0; i < 4; ++i)
            {
                for (int j = 0; j < 4; ++j, ++n)
                {
                    if (flag ==1 && arr[n] != _gameField[j, i])
                    {
                        return false;
                    }
                    if (flag == 2 && arr[n] != _gameField[i, j])
                    {
                        return false;
                    }
                    if ((flag == 3 && arr[n] != _gameField[j, i]) |
                            (flag == 3 && arr[n] != _gameField[i, j]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

    }
}
