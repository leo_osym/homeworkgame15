﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game15.App
{
    public partial class ResultsTable : Form
    {
        public ResultsTable()
        {
            InitializeComponent();
        }
        public void SetLabels(string[] arr)
        {
            labelTime.ForeColor = Color.Green;
            labelDate.ForeColor = Color.Green;
            labelPlayerName.ForeColor = Color.Green;
            labelDate.Text += "\n"+arr[0];
            labelPlayerName.Text += "\n"+arr[1];
            labelTime.Text += "\n"+arr[2];
        }
    }
}
