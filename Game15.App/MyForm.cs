﻿using GameLogic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game15.App
{
    public partial class MyForm : Form
    {
        StartForm dlg = new StartForm();
        Game game = new Game(valContainer.typeOfGame);

        int timeSpent;
        int btnSize;
        int btnMargin=10;
        public MyForm() 
        {
            InitializeComponent();
            btnSize = ClientSize.Width > ClientSize.Height ? 
                (ClientSize.Height - btnMargin) / 4 - btnMargin :
                (ClientSize.Height - btnMargin) / 4 - btnMargin;
            gameTimer.Tick += gameTimer_Tick;
            gameTimer.Interval = 4000; // problem; don't know how to set up it correctly
            gameTimer.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Show();
            if (dlg.ShowDialog(this) == DialogResult.Cancel)
            {
                Close();
                return;
            }
            game.InitGame(valContainer.typeOfGame);

            for(int i =0; i<4; ++i)
            {
                for(int j =0; j<4; ++j)
                {
                    if (game[i, j] != 0)
                        new Button
                        {
                            Parent = this,
                            Text = Convert.ToString(game[i, j],10),
                            Tag = game[i, j],
                            Width = btnSize,
                            Height = btnSize,
                            Location = new Point
                            {
                                X = btnMargin + (btnSize + btnMargin) * i,
                                Y = btnMargin + (btnSize + btnMargin) * j,
                            }
                        }.Click += btnClick;
                }
            }
        }

        private void btnClick(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn == null || btn.Tag == null) return;
            int value = (int)btn.Tag;
            int x = game.zeroX;
            int y = game.zeroY;
            if(game.CheckAndGo(value))
            {
                btn.Location = new Point
                {
                    X = btnMargin + (btnSize + btnMargin) * x,
                    Y = btnMargin + (btnSize + btnMargin) * y,
                };
            }
            if(game.IsWin(valContainer.typeOfGame))
            {
                gameTimer.Stop();
                TimeSpan time = TimeSpan.FromSeconds(timeSpent);
                valContainer.playerTime = time.ToString();

                AddToTable();
                MessageBox.Show($"You win, {valContainer.playerName}! \n Your time: {time.ToString()}");
                ShowTable();
            }
        }

        private void ShowTable()
        {
            ResultsTable tbl = new ResultsTable();
            tbl.Show();
            try
            {
                using (StreamReader r = new StreamReader(@"E:\Projects\csharp\CSHARP_COURSE\Game15\Game15.App\res\results_table.txt"))
                {
                    //do staff
                    string line;
                    string[] comp = new string[3];
                    while((line = r.ReadLine())!=null)
                    {
                        comp = line.Split(new char[] { '<', '>' }, StringSplitOptions.RemoveEmptyEntries);
                        tbl.SetLabels(comp);
                    }
                }
            }
            catch
            {
                MessageBox.Show("File 'table_result.txt' doesn't exist!");
            }
        }

        private static void AddToTable()
        {
            try
            {
                string dt = DateTime.Now.ToString("yyyy-MM-dd, hh:mm");
                using (StreamWriter w = File.AppendText(@"E:\Projects\csharp\CSHARP_COURSE\Game15\Game15.App\res\results_table.txt"))
                {
                    w.WriteLine($"<{dt}><{valContainer.playerName}><{valContainer.playerTime}>");
                }
            }
            catch
            {
                MessageBox.Show("File 'table_result.txt' doesn't exist!");
            }
        }

        private void gameTimer_Tick(object sender, EventArgs e)
        {
            timeSpent++;
        }
    }
}
