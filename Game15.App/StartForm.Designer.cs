﻿namespace Game15.App
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.horizontalAxis = new System.Windows.Forms.RadioButton();
            this.verticalAxis = new System.Windows.Forms.RadioButton();
            this.doubleAxis = new System.Windows.Forms.RadioButton();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Введите имя:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(104, 9);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(173, 20);
            this.textBoxName.TabIndex = 1;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // horizontalAxis
            // 
            this.horizontalAxis.AutoSize = true;
            this.horizontalAxis.Location = new System.Drawing.Point(15, 56);
            this.horizontalAxis.Name = "horizontalAxis";
            this.horizontalAxis.Size = new System.Drawing.Size(108, 17);
            this.horizontalAxis.TabIndex = 2;
            this.horizontalAxis.TabStop = true;
            this.horizontalAxis.Text = "Горизонтальная";
            this.horizontalAxis.UseVisualStyleBackColor = true;
            this.horizontalAxis.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // verticalAxis
            // 
            this.verticalAxis.AutoSize = true;
            this.verticalAxis.Location = new System.Drawing.Point(15, 79);
            this.verticalAxis.Name = "verticalAxis";
            this.verticalAxis.Size = new System.Drawing.Size(97, 17);
            this.verticalAxis.TabIndex = 3;
            this.verticalAxis.TabStop = true;
            this.verticalAxis.Text = "Вертикальная";
            this.verticalAxis.UseVisualStyleBackColor = true;
            this.verticalAxis.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // doubleAxis
            // 
            this.doubleAxis.AutoSize = true;
            this.doubleAxis.Location = new System.Drawing.Point(15, 102);
            this.doubleAxis.Name = "doubleAxis";
            this.doubleAxis.Size = new System.Drawing.Size(98, 17);
            this.doubleAxis.TabIndex = 4;
            this.doubleAxis.TabStop = true;
            this.doubleAxis.Text = "Двусторонняя";
            this.doubleAxis.UseVisualStyleBackColor = true;
            this.doubleAxis.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(134, 114);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 5;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(215, 114);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Выберите тип игры:";
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(302, 149);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.doubleAxis);
            this.Controls.Add(this.verticalAxis);
            this.Controls.Add(this.horizontalAxis);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.label1);
            this.Name = "StartForm";
            this.Text = "StartForm";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.RadioButton doubleAxis;
        private System.Windows.Forms.RadioButton verticalAxis;
        private System.Windows.Forms.RadioButton horizontalAxis;
        private System.Windows.Forms.Label label2;
    }
}